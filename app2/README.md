# app2
Modyfikacja app1 umożliwiające pracę nad kodem bez ciągłego przebudowania obrazów.

## Modyfikacja docker-compose.yml
Dodanie
```
volumes:
  - .:/code
environment:
  FLASK_ENV: development
```

## Uruchomienie
```
docker-compose up
```

# Możliwość uruchomienia komend w kontenerach
```
docker-compose run web env
```
