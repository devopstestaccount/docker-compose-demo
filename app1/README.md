# app1
Przykład zastosowania docker-compose

## Utworzenie i uruchomienie kontenerów
`` `
docker-compose up
```
## Uruchomienie w trybie daemon
```
docker-compose up -d
```

## Wymuszenie z budowania obrazów
```
docker-compose up --build -d
```

## Lista uruchomienych serwisów
```
docker-compose ps
```
vs 
```
docker ps
```

## Podgląd logów np z redis
```
docker-compose logs -f redis
```

## Zatrzymanie i usunięcie kontenerów
```
docker-compose down
```

## Zatrzymanie kontenerów
```
docker-compose stop
```
